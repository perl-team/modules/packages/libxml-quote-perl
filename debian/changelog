libxml-quote-perl (1.02-5) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/control: update Build-Depends for cross builds.
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Fix day-of-week for changelog entry 1.02-1.
  * Bump debhelper from old 12 to 13.

  [ gregor herrmann ]
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.6.1.
  * Enable all hardening flags.
  * Add debian/tests/pkg-perl/use-name for autopkgtests.

 -- gregor herrmann <gregoa@debian.org>  Sun, 14 Aug 2022 21:19:53 +0200

libxml-quote-perl (1.02-4) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Benoit Mortier from Uploaders. Thanks for your work!
  * Remove Ryan Niebur from Uploaders. Thanks for your work!
  * Add Testsuite declaration for autopkgtest-pkg-perl.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Niko Tyni ]
  * Update to debhelper compat level 10
  * Update to Standards-Version 4.1.3
  * Declare that the package does not need (fake)root to build

 -- Niko Tyni <ntyni@debian.org>  Sat, 31 Mar 2018 14:26:36 +0300

libxml-quote-perl (1.02-3) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ intrigeri ]
  * Bump debhelper compat level to 9.
  * Declare compatibility with Standards-Version 3.9.5.
  * Adjust versioned Build-Depends on debhelper to (>= 9.20120312~)
    to get hardening build flags.

 -- intrigeri <intrigeri@debian.org>  Fri, 11 Apr 2014 22:26:14 +0200

libxml-quote-perl (1.02-2) unstable; urgency=low

  [ Benoit Mortier ]
  * Added perl group as maintainer
  * correct vcs-svn and vcs-browse
  * bumped policy to latest

  [ gregor herrmann ]
  * debian/watch: update to ignore development releases.
  * Add /me to Uploaders.
  * Set Standards-Version to 3.9.1; remove version from perl build dependency.
  * debhelper 7.
  * debian/copyright: DEP5 format.
  * Convert to source format 3.0 (quilt).

 -- gregor herrmann <gregoa@debian.org>  Tue, 27 Jul 2010 22:31:16 -0400

libxml-quote-perl (1.02-1) unstable; urgency=low

  * Initial upload (Closes: #532684)

 -- Cajus Pollmeier <cajus@debian.org>  Wed, 24 Jun 2009 10:18:00 +0200
